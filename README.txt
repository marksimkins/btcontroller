BTController

This is a silly little prototype Android application, where I integrated a little bluetooth controller into a pointless little game. More just to explore the interface than do anything revolutionary. It's horribly basic in its implementation, and not everything I wrote from scratch.

So, you start with a circle on a clear canvas. With a suitable bluetooth controller, you have the following options:

Analogue stick: move the circle around
X: shrink the circle
Y: grow the circle
A: change the circle to the next colour
B: change the circle to the previous colour

As this is a silly prototype, I won't bother with a branching strategy.
