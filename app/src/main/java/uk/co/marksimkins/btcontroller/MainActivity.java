package uk.co.marksimkins.btcontroller;

import android.graphics.Point;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Display;
import android.view.InputDevice;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.SurfaceView;

import uk.co.marksimkins.btcontroller.engine.GameEngineFactory;

/**
 * The main activity of the application.
 * <br/>
 * This quite simply starts the game engine, creates the canvas onto which the game is rendered,
 * and responds to input requests from the bluetooth controller.
 */
public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        GameEngineFactory.initEngine(size.x, size.y);

        //sets the activity view as GameView class
        SurfaceView view = new GameView(this);
        setContentView(view);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        // Handle button presses
        if ((event.getSource() & InputDevice.SOURCE_GAMEPAD) == InputDevice.SOURCE_GAMEPAD) {
             switch (keyCode) {
                case KeyEvent.KEYCODE_BUTTON_A:
                    GameEngineFactory.getEngine().setNextColour();
                    return true;
                case KeyEvent.KEYCODE_BUTTON_B:
                    GameEngineFactory.getEngine().setPreviousColour();
                    return true;
                case KeyEvent.KEYCODE_BUTTON_Y:
                    GameEngineFactory.getEngine().growCircle();
                    return true;
                case KeyEvent.KEYCODE_BUTTON_X:
                    GameEngineFactory.getEngine().shrinkCircle();
                    return true;
            }
        }
        return false;
    }

    @Override
    public boolean onGenericMotionEvent(MotionEvent event) {
        // Check that the event came from a game controller
        if ((event.getSource() & InputDevice.SOURCE_JOYSTICK) == InputDevice.SOURCE_JOYSTICK) {

            final int historySize = event.getHistorySize();

            for (int i = 0; i < historySize; i++) {
                processJoystickInput(event, i);
            }

            // Process the current movement sample in the batch (position -1)
            processJoystickInput(event, -1);
            return true;
        }
        return super.onGenericMotionEvent(event);
    }

    /**
     * Do something with the joystick input.
     *
     * @param event         The MotionEvent object describing the joystick event received.
     * @param historyPos    At which numerical point in the history of events this was. -1 = now.
     */
    private void processJoystickInput(MotionEvent event, int historyPos) {
        InputDevice mInputDevice = event.getDevice();

        // Calculate the horizontal distance to move
        float xPosCtrl = getCenteredAxis(event, mInputDevice, MotionEvent.AXIS_X, historyPos);
        if (xPosCtrl == 0) {
            xPosCtrl = getCenteredAxis(event, mInputDevice, MotionEvent.AXIS_HAT_X, historyPos);
        }
        if (xPosCtrl == 0) {
            xPosCtrl = getCenteredAxis(event, mInputDevice, MotionEvent.AXIS_Z, historyPos);
        }
        GameEngineFactory.getEngine().updateXModifier(xPosCtrl);

        // Calculate the vertical distance to move
        float yPosCtrl = getCenteredAxis(event, mInputDevice, MotionEvent.AXIS_Y, historyPos);
        if (yPosCtrl == 0) {
            yPosCtrl = getCenteredAxis(event, mInputDevice, MotionEvent.AXIS_HAT_Y, historyPos);
        }
        if (yPosCtrl == 0) {
            yPosCtrl = getCenteredAxis(event, mInputDevice, MotionEvent.AXIS_RZ, historyPos);
        }
        GameEngineFactory.getEngine().updateYModifier(yPosCtrl);
    }

    /**
     * Calculate the distance moved in the given axis, considering the device's specific 'flat' range.
     *
     * @param event         The MotionEvent object describing the joystick event received.
     * @param device        The InputDevice being used, with its own range of motion.
     * @param axis          The MotionEvent axis of movement.
     * @param historyPos    At which numerical point in the history of events this was. -1 = now.
     * @return 0 if the joystick is at rest, otherwise a float value describing the distance moved in the desired axis.
     */
    private static float getCenteredAxis(MotionEvent event, InputDevice device, int axis, int historyPos) {
        final InputDevice.MotionRange range = device.getMotionRange(axis, event.getSource());

        // A joystick at rest does not always report an absolute position of
        // (0,0). Use the getFlat() method to determine the range of values
        // bounding the joystick axis center.
        if (range != null) {
            final float flat = range.getFlat();
            final float value = historyPos < 0 ? event.getAxisValue(axis):
                            event.getHistoricalAxisValue(axis, historyPos);

            // Ignore axis values that are within the 'flat' region of the joystick axis center.
            if (Math.abs(value) > flat) {
                return value;
            }
        }
        return 0;
    }
}
