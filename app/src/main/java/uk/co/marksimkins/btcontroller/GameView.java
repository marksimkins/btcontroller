package uk.co.marksimkins.btcontroller;

import android.content.Context;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

/**
 * This is just the surface onto which the circles are drawn.
 * <br/>
 * It spawns a separate thread to draw the circle in the appropriate position, colour, and size.
 */
public class GameView extends SurfaceView implements SurfaceHolder.Callback {

    /** The separate thread that will handle the rendering of the circle. */
    private DisplayThread displayThread;

    public GameView(Context context) {
        super(context);
        SurfaceHolder holder = getHolder();
        holder.addCallback(this);

        displayThread = new DisplayThread(holder);
        setFocusable(true);
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        //Starts the display thread
        if(!displayThread.isRunning()) {
            displayThread = new DisplayThread(holder);
            displayThread.start();
        } else {
            displayThread.start();
        }
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        // do nothing
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        //Stop the display thread
        displayThread.setIsRunning(false);
        try {
            displayThread.join();
        } catch (InterruptedException e) {
            // should really do something with this
        }
    }

}
