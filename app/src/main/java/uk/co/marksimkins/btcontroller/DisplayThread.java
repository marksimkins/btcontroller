package uk.co.marksimkins.btcontroller;

import android.graphics.Canvas;
import android.view.SurfaceHolder;

import uk.co.marksimkins.btcontroller.engine.GameEngineFactory;

/**
 * This thread handles the rendering of the circle, separate from the main UI thread.
 */
public class DisplayThread extends Thread {

    /** Arbitrary delay in millis to avoid excessive processing as fast as the CPU can handle. */
    private static final long DELAY = 30;

    /** Do we want to keep this thread running? */
    private boolean isRunning = true;

    /** The surface onto which we want to draw the circle. */
    private SurfaceHolder surfaceHolder;

    public DisplayThread(SurfaceHolder surfaceHolder) {
        this.surfaceHolder = surfaceHolder;
    }

    @Override
    public void run() {
        // Looping until the boolean is false
        while (isRunning) {
            // Updates the position of the circle
            GameEngineFactory.getEngine().update();

            // Lock the canvas for drawing. If it's null, it's either not created, or being drawn to by another process.
            Canvas canvas = surfaceHolder.lockCanvas(null);

            if (canvas != null) {
                // Clears the screen with white paint and draws the circle on the canvas
                synchronized (surfaceHolder) {
                    GameEngineFactory.getEngine().draw(canvas);
                }
                // Unlock the canvas
                surfaceHolder.unlockCanvasAndPost(canvas);
            }
            // Delay a little per frame
            try {
                Thread.sleep(DELAY);
            } catch (InterruptedException ex) {}
        }
    }

    /**
     * @return Is this thread currently running?
     */
    public boolean isRunning() {
        return isRunning;
    }

    /**
     * Set whether we want to continue running this thread.
     * @param running true if you want it to run, or false if you want it to stop.
     */
    public void setIsRunning(boolean running) {
        isRunning = running;
    }
}
