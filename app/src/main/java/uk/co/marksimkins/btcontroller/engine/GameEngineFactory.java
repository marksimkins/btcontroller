package uk.co.marksimkins.btcontroller.engine;

/**
 * A GameEngine factory that creates one GameEngine instance based upon the display size, which can
 * then be retrieved from anywhere.
 */
public class GameEngineFactory {

    /** The single GameEngine instance. */
    private static GameEngine engine;

    /**
     * Initialise me a new GameEngine with the following dimensions please!
     *
     * @param width     The width of the canvas
     * @param height    The height of the canvas
     */
    public static void initEngine(float width, float height) {
        engine = new GameEngine(width, height);
    }

    /**
     * @return The GameEngine. May be null if initEngine has not been called.
     */
    public static GameEngine getEngine() {
        return engine;
    }

}
