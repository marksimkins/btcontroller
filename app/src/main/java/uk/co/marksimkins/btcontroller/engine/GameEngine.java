package uk.co.marksimkins.btcontroller.engine;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;

/**
 * The circle game engine itself.
 * <br/>
 * It's nothing all that fancy, it simply tracks the position of a circle within the canvas, not allowing
 * it to go outside of it, and updates its position. It also supports scaling of the circle within
 * set bounds, and cycling through a few colours.
 */
public class GameEngine {

    /** The available circle colours we can cycle through. */
    private static final int[] colours = new int[] {
            Color.RED, Color.GREEN, Color.BLUE, Color.YELLOW, Color.BLACK
    };

    /**
     * If we used the pure X/Y inputs from the joystick to move the circle, it'd move VERY slowly.
     * So we multiple those values with this one, to speed it up a bit.
     */
    private static final int SPEED_MODIFIER = 30;

    /**
     * How much we want to increase or decrease the circle size by, each time the appropriate button is pressed.
     */
    private static final int CIRCLE_SIZE_MODIFIER = 10;

    /**
     * The circle won't get any smaller than this.
     * In case you're interested, the circle won't get any bigger than the screen width either.
     */
    private static final int MIN_CIRCLE_SIZE = 10;

    /** The width of the screen. */
    private final float screenWidth;

    /** The height of the screen. */
    private final float screenHeight;

    /** Which colour in the array of available colours we're currently rendering. */
    private int currentColourPos = 0;

    /** The radius of the circle to draw. */
    private int circleRadius = 100;

    /** The current X-coordinate of the circle. */
    private float xPos;
    /** How much we want to modify the X coordinate each frame. */
    private float xPosMod;

    /** The current Y-coordinate of the circle. */
    private float yPos;
    /** How much we want to modify the Y coordinate each frame. */
    private float yPosMod;

    /**
     * Create a new GameEngine with the following dimensions.
     *
     * @param width     The width of the canvas
     * @param height    The height of the canvas
     */
    GameEngine(float width, float height) {
        this.screenWidth = width;
        this.screenHeight = height - 243; // just remove some for the nav, can't be bothered to get this properly
        this.xPos = width / 2;
        this.yPos = height / 2;
    }

    /**
     * Update the position of the circle, based upon any changes to inputs already received.
     */
    public void update() {
        if (xPosMod > 0 && xPos >= getMaxPos(screenWidth)) {
            xPosMod = 0;
            xPos = getMaxPos(screenWidth);
        } else if (xPosMod < 0 && xPos <= getMinPos()) {
            xPosMod = 0;
            xPos = getMinPos();
        }

        if (yPosMod > 0 && yPos >= getMaxPos(screenHeight)) {
            yPosMod = 0;
            yPos = getMaxPos(screenHeight);
        } else if (yPosMod < 0 && yPos <= getMinPos()) {
            yPosMod = 0;
            yPos = getMinPos();
        }

        xPos += xPosMod;
        yPos += yPosMod;
    }

    /**
     * Draw the circle in the appropriate position, size, and colour, on the provided Canvas.
     *
     * @param canvas What we want to draw the circle onto.
     */
    public void draw(Canvas canvas) {
        Paint paint = new Paint();
        paint.setStyle(Paint.Style.FILL);
        paint.setColor(Color.WHITE);
        canvas.drawPaint(paint);
        paint.setColor(colours[currentColourPos]);
        canvas.drawCircle(xPos, yPos, circleRadius, paint);
    }

    /**
     * Change the circle colour to the next available colour.
     */
    public void setNextColour() {
        currentColourPos++;
        if (currentColourPos >= colours.length) {
            currentColourPos = 0;
        }
    }

    /**
     * Change the circle colour to the previous colour.
     */
    public void setPreviousColour() {
        currentColourPos--;
        if (currentColourPos < 0) {
            currentColourPos = colours.length - 1;
        }
    }

    /**
     * Make the circle bigger.
     */
    public void growCircle() {
        circleRadius += CIRCLE_SIZE_MODIFIER;
        if (circleRadius >= (screenWidth / 2)) {
            circleRadius = (int) (screenWidth / 2);
        }
    }

    /**
     * Shrink the circle.
     */
    public void shrinkCircle() {
        circleRadius -= CIRCLE_SIZE_MODIFIER;
        if (circleRadius <= MIN_CIRCLE_SIZE) {
            circleRadius = MIN_CIRCLE_SIZE;
        }
    }

    /**
     * Update the value we will use to modify the X-position of the circle.
     *
     * @param x How much we want to move the circle in the X axis.
     */
    public void updateXModifier(float x) {
        xPosMod = x * SPEED_MODIFIER;
    }

    /**
     * Update the value we will use to modify the Y-position of the circle.
     *
     * @param y How much we want to move the circle in the Y axis.
     */
    public void updateYModifier(float y) {
        yPosMod = y * SPEED_MODIFIER;
    }

    /**
     * @return The closest the circle can get to the 0-pos walls - which happens to be the circle's radius.
     */
    private float getMinPos() {
        return circleRadius;
    }

    /**
     * How close the circle can get to the outer walls - which is the width or height, minus the circle radius.
     *
     * @param max The max dimentions the circle could possibly reach, e.g. width, or height
     * @return The maximum allowable position for the circle
     */
    private float getMaxPos(float max) {
        return max - circleRadius;
    }
}
